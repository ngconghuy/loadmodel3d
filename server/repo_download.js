/**
 * Created by Creepy on 10/11/2016.
 */
var superagent = require('superagent');
var cheerio = require('cheerio');
var fs = require('fs-extra');
var request = require('request').defaults({encoding: null});
DownloadModel = function () {
    superagent.get('http://www.lolking.net/models')
        .end(function (err, res) {
            // var championSkins;
            if (err) return;
            var html = res.text;
            var $ = cheerio.load(html);
            var scriptContent = $('div > script').eq(10).text();
            var filter = scriptContent.match(/var championSkins[\s\S]*\$\(\'\[name=\"champion\"\]\'\)\.change/)[0];
            eval(filter);
            var opts = {};
            // console.log(championSkins);

            Object.each(championSkins, function (keyChamp, valueChamp) {
                Object.each(valueChamp, function (keySkin, valueSkin) {
                    opts.models = opts.models || [];
                    opts.models.push({champion: keyChamp, skin: keySkin});
                    if (keyChamp === 1) {
                        opts.models.push({champion: 'annietibbers', skin: type});
                    } else if (keyChamp === 74) {
                        opts.models.push({champion: 'heimertblue', skin: type}, {champion: 'heimertgreen', skin: type},
                            {champion: 'heimertred', skin: type}, {champion: 'heimertyellow', skin: type});
                    } else if (keyChamp === 117) {
                        opts.models.push({champion: 'lulufaerie', skin: type});
                    } else if (keyChamp === 102) {
                        opts.models.push({champion: 'shyvanadragon', skin: type});
                    } else if (keyChamp === 90) {
                        opts.models.push({champion: 'malzaharvoidling', skin: type});
                    } else if (keyChamp === 76) {
                        opts.models.push({champion: 'nidaleecougar', skin: type});
                    } else if (keyChamp === 57) {
                        opts.models.push({champion: 'maokaisproutling', skin: type});
                    } else if (keyChamp === 50) {
                        opts.models.push({champion: 'swainraven', skin: type});
                    } else if (keyChamp === 35) {
                        opts.models.push({champion: 'shacobox', skin: type});
                    } else if (keyChamp === 17) {
                        opts.models.push({champion: 'teemomushroom', skin: type});
                    } else if (keyChamp === 133) {
                        opts.models.push({champion: 'quinnvalor', skin: type});
                    } else if (keyChamp === 60) {
                        opts.models.push({champion: 'elisespider', skin: type});
                    } else if (keyChamp === 150) {
                        opts.models.push({champion: 'gnarbig', skin: type});
                    } else if (keyChamp === 37 && type == 6) {
                        opts.models = [];
                        opts.models.push({champion: 'sonadjgenre02', skin: type});
                    }
                    //         // if (keyChamp == 1) {
                    //         //     opts.models.push({champion: 'annietibbers', skin: keySkin});
                    //         // } else if (keyChamp == 74) {
                    //         //     opts.models.push({champion: 'heimertblue', skin: keySkin}, {
                    //         //             champion: 'heimertgreen',
                    //         //             skin: keySkin
                    //         //         },
                    //         //         {champion: 'heimertred', skin: keySkin}, {champion: 'heimertyellow', skin: keySkin});
                    //         // } else if (keyChamp == 117) {
                    //         //     opts.models.push({champion: 'lulufaerie', skin: keySkin});
                    //         // } else if (keyChamp == 102) {
                    //         //     opts.models.push({champion: 'shyvanadragon', skin: keySkin});
                    //         // } else if (keyChamp == 90) {
                    //         //     opts.models.push({champion: 'malzaharvoidling', skin: keySkin});
                    //         // } else if (keyChamp == 77) {
                    //         //     opts.models.push({champion: 'udyrphoenix', skin: keySkin}, {
                    //         //             champion: 'udyrtiger',
                    //         //             skin: keySkin
                    //         //         },
                    //         //         {champion: 'udyrturtle', skin: keySkin});
                    //         // } else if (keyChamp == 76) {
                    //         //     opts.models.push({champion: 'nidalee_cougar', skin: keySkin});
                    //         // } else if (keyChamp == 57) {
                    //         //     opts.models.push({champion: 'maokaisproutling', skin: keySkin});
                    //         // } else if (keyChamp == 50) {
                    //         //     opts.models.push({champion: 'swainraven', skin: keySkin});
                    //         // } else if (keyChamp == 35) {
                    //         //     opts.models.push({champion: 'shacobox', skin: keySkin});
                    //         // } else if (keyChamp == 17) {
                    //         //     opts.models.push({champion: 'teemomushroom', skin: keySkin});
                    //         // } else if (keyChamp == 133) {
                    //         //     opts.models.push({champion: 'quinnvalor', skin: keySkin});
                    //         // } else if (keyChamp == 60) {
                    //         //     opts.models.push({champion: 'elisespider', skin: keySkin});
                    //         // } else if (keyChamp == 150) {
                    //         //     opts.models.push({champion: 'gnarbig', skin: keySkin})
                    //         // }
                })
            });
            // console.log(opts.models);
            opts.models.forEach(function (item) {
                load(item.champion, item.skin)
            });
        });

    function load(champion, skin) {
        var self = this;
        self.champion = champion;
        self.skin = skin;
        var url = 'http://media.services.zam.com/v1/media/byName/lol/mv/models/' + champion + "_" + skin + ".lmesh";
        request.get(url, function (err, res, body) {
            var pathLmesh = 'C:\\Users\\Creepy\\WebstormProjects\\viewer\\public\\lmesh\\' + champion + '_' + skin + '.lmesh';
            var buff = Buffer.from(body);

            function getString(buffer, offset) {
                var len = buffer.readUInt16LE(offset);
                offset += 2;
                var str = "";
                for (var i = 0; i < len; i++) {
                    str += String.fromCharCode(buffer.readUInt8(offset + i));
                }
                return str;
            }

            try {
                var animFile = getString(buff, 8);
                var textureOffset = 8 + 2 + animFile.length;
                var textureFIle = getString(buff, textureOffset);
                console.log(champion, animFile, textureFIle);
                loadMesh(champion, animFile, textureFIle)
            }
            catch (e) {
                return console.log(champion, 'mesh error');
            }
            fs.outputFile(pathLmesh, body, function (err) {
                if (!err) console.log('success lmesh');
            });
        });
    }

    function loadMesh(champ, animFile, textureFIle) {
        var urlanimFile = 'http://media.services.zam.com/v1/media/byName/lol/mv/models/' + animFile + '.lanim';
        var urlpngFile = 'http://media.services.zam.com/v1/media/byName/lol/mv/textures/' + champ + '/' + textureFIle + '.png';
        request.get(urlanimFile, function (err, res, body) {
            var pathanimFile = 'C:\\Users\\Creepy\\WebstormProjects\\viewer\\public\\anim\\' + animFile + '.lanim';
            fs.outputFile(pathanimFile, body, function (err) {
                if (!err) console.log('success animFile');
            });
        });
        request.get(urlpngFile, function (err, res, body) {
            var pathurlpngFile = 'C:\\Users\\Creepy\\WebstormProjects\\viewer\\public\\png\\' + textureFIle + '.png';
            fs.outputFile(pathurlpngFile, body, function (err) {
                if (!err) console.log('success pngFile');
            });
        });
    }
};
